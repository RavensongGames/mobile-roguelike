﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	
	Vector3 drag_origin;
	Vector3 drag_diff;
	bool dragging = false;
	float scrollAmount = 0f;
	Camera myCam;

	void Start () {
		myCam = GetComponent<Camera>();
	}

	void LateUpdate () {
		MouseDrag();
		ScrollZoom();
	}

	void MouseDrag() {
		if (Input.GetMouseButton(0)) {
			drag_diff = (myCam.ScreenToWorldPoint(Input.mousePosition)) - myCam.transform.position;
			if (!dragging) {
				dragging = true;
				drag_origin = myCam.ScreenToWorldPoint(Input.mousePosition);
			}
		} else
			dragging = false;

		if (dragging)
			Camera.main.transform.position = drag_origin - drag_diff;
	}

	void ScrollZoom() {
		scrollAmount = Input.GetAxis("Mouse ScrollWheel");

		if (scrollAmount != 0) {
			myCam.orthographicSize -= scrollAmount * 5f;
			myCam.orthographicSize = Mathf.Clamp(myCam.orthographicSize, 4f, 25f);
		}
	}

	void PinchZoom() {
		if (Input.touchCount == 2) {
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			myCam.orthographicSize += deltaMagnitudeDiff * 0.5f;
			myCam.orthographicSize = Mathf.Max(myCam.orthographicSize, 1f);
		}
	}
}