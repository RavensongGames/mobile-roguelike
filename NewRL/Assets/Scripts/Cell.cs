﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell {

	public int tileID;
	public int x;
	public int y;
	public bool walkable;
	public Entity entity;
	public bool lit;
	public bool hasSeen;

	public Cell() {
		tileID = 0;
		x = 0;
		y = 0;
		walkable = true;
		entity = null;
	}

	public Cell(int id, int px, int py) {
		tileID = id;
		x = px;
		y = py;
		walkable = true;
		entity = null;
	}

	public bool Walkable() {
		return (walkable && (entity == null || !entity.blocksMovement));
	}

	public void SetEntity(Entity ent) {
		if (!walkable)
			return;
		
		entity = ent;
		entity.cell = this;
		EntityManager.instance.SetPosition(ent);
	}

	public void UnSetEntity() {
		if (entity != null) {
			entity.cell = null;
			entity = null;
		}
	}
}
