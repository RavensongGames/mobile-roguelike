﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity {

	public string Name;
	public int posX;
	public int posY;
	public Stats stats;
	public Cell cell;
	public bool blocksMovement = true;
	public bool canAct = true;

	public Entity(string name, int px, int py, Stats st, bool blocks = true) {
		this.Name = name;
		stats = st;
		stats.entity = this;
		blocksMovement = blocks;
		SetPosition(px, py);
	}

	public Entity(string Name, Cell c, Stats st, bool blocks = true) {
		this.Name = Name;
		this.stats = st;
		stats.entity = this;
		blocksMovement = blocks;
		SetPosition(c);
	}

	public void StartTurn() {
		if (canAct && this != EntityManager.playerInstance.Key)
			Act(Random.Range(-1, 2), Random.Range(-1, 2));
	}

	public bool Act(int xDir, int yDir) {
		if (xDir == 0 && yDir == 0) {
			Wait();
			return true;
		}

		Cell c = Map.instance.GetTileAt(posX + xDir, posY + yDir);

		if (!c.Walkable()) {
			if (c.entity != null && c.entity.stats != null) {
				Attack(c.entity.stats);
				return true;
			}
		} else {
			Move(c, xDir, yDir);
			return true;
		}

		return false;
	}

	public void SetPosition(int x, int y) {
		if (cell != null)
			cell.UnSetEntity();
		
		posX = x;
		posY = y;

		Map.instance.GetTileAt(posX, posY).SetEntity(this);
	}

	public void SetPosition(Cell c) {
		SetPosition(c.x, c.y);
	}

	void Attack(Stats s) {
		if (this == EntityManager.playerInstance.Key || s.entity == EntityManager.playerInstance.Key)
			s.TakeDamage(stats.strength, this);
	}

	void Wait() {
		if (stats.HP < stats.maxHP)
			stats.HP ++;
	}

	void Move(Cell c, int xMove, int yMove) {
		if (cell != null)
			cell.UnSetEntity();
		
		posX += xMove;
		posY += yMove;

		c.SetEntity(this);
	}

	public void Die() {
		MessageLog.instance.NewMessage(Name + " dies!");

		canAct = false;
		stats = null;
		blocksMovement = false;
		Name = Name + "'s corpse";
		EntityManager.instance.RemoveEntity(this);
	}
}
