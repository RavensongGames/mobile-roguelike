﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityManager : MonoBehaviour {

	public static EntityManager instance;
	public static KeyValuePair<Entity, GameObject> playerInstance;

	public Dictionary<Entity, GameObject> entities;
	public GameObject entityGO;
	public Sprite deadSprite;

	public void Init() {
		instance = this;
		entities = new Dictionary<Entity, GameObject>();

		Stats s = new Stats(10, 0, 2);
		Cell cell = MapManager.instance.GetOpenTile();
		Entity player = new Entity("Player", cell, s);
		GameObject playerObject = NewEntity(player);
		playerInstance = new KeyValuePair<Entity, GameObject>(player, playerObject);

		for (int i = 0; i < 4; i++) {
			Stats s2 = new Stats(10, 0, 1);
			Cell c = MapManager.instance.GetOpenTile();
			Entity npc = new Entity("NPC", c, s2);
			NewEntity(npc);
			entities[npc].GetComponent<SpriteRenderer>().color = Color.red;
		}

		CheckInSightEntities();
		Camera.main.transform.position = new Vector3(playerInstance.Key.posX, playerInstance.Key.posY, Camera.main.transform.position.z);
	}

	public void SetPosition(Entity ent) {
		if (entities.ContainsKey(ent))
			entities[ent].transform.position = new Vector3(ent.posX, ent.posY, 0);
		else
			NewEntity(ent);
	}

	public GameObject NewEntity(Entity ent) {
		if (!entities.ContainsKey(ent)) {
			GameObject newGO = (GameObject)Instantiate(entityGO, new Vector3(ent.posX, ent.posY, 0), Quaternion.identity, this.transform);
			entities.Add(ent, newGO);

			return newGO;
		}
			
		return null;
	}

	public void RemoveEntity(Entity ent) {
		if (entities.ContainsKey(ent)) {
			entities[ent].GetComponent<SpriteRenderer>().sprite = deadSprite;
			entities[ent].GetComponent<SpriteRenderer>().sortingOrder = -1;
		}
	}

	void Update() {
		HandleInput();
	}

	void HandleInput() {
		if (Input.GetKeyDown(KeyCode.Keypad8))  //N
			PlayerAction(0, 1);
		if (Input.GetKeyDown(KeyCode.Keypad6))  //E
			PlayerAction(1, 0);
		if (Input.GetKeyDown(KeyCode.Keypad2))  //S
			PlayerAction(0, -1);
		if (Input.GetKeyDown(KeyCode.Keypad4))  //W
			PlayerAction(-1, 0);
		if (Input.GetKeyDown(KeyCode.Keypad9))  //NE
			PlayerAction(1, 1);
		if (Input.GetKeyDown(KeyCode.Keypad3))  //SE
			PlayerAction(1, -1);
		if (Input.GetKeyDown(KeyCode.Keypad1))  //SW
			PlayerAction(-1, -1);
		if (Input.GetKeyDown(KeyCode.Keypad7))  //NW
			PlayerAction(-1, 1);
		if (Input.GetKeyDown(KeyCode.Keypad5)) //Wait
			PlayerAction(0, 0);
	}

	public void PlayerAction(string dir) {
		if (dir == "N") PlayerAction(0, 1);
		else if (dir == "S") PlayerAction(0, -1);
		else if (dir == "E") PlayerAction(1, 0);
		else if (dir == "W") PlayerAction(-1, 0);

		else if (dir == "NE") PlayerAction(1, 1);
		else if (dir == "SE") PlayerAction(1, -1);
		else if (dir == "NW") PlayerAction(-1, 1);
		else if (dir == "SW") PlayerAction(-1, -1);

		else if (dir == "Wait") PlayerAction(0, 0);
	}

	void PlayerAction(int x, int y) {
		if (playerInstance.Key.Act(x, y)) {
			foreach (Entity en in entities.Keys) {
				if (en != playerInstance.Key)
					en.StartTurn();
			}
		}

		Camera.main.transform.position = new Vector3(playerInstance.Key.posX, playerInstance.Key.posY, Camera.main.transform.position.z);
		CheckInSightEntities();
	}

	void CheckInSightEntities() {
		MapManager.instance.LOSCalc();

		foreach (Entity en in entities.Keys) {
			if (en != null && en != playerInstance.Key)
				entities[en].GetComponent<SpriteRenderer>().enabled = en.cell.lit;
		}
	}
}
