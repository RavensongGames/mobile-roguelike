﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generator {
	public class Room {
		public int left;
		public int bottom;
		public int width;
		public int height;
		public bool connected = false;

		public int right {
			get { return left + width; }
		}

		public int top {
			get { return bottom + height; }
		}

		public int centerX {
			get { return left + width / 2; }
		}

		public int centerY {
			get { return bottom + height / 2; }
		}

		public Room(int l, int b, int w, int h) {
			left = l;
			bottom = b;
			width = w;
			height = h;
		}

		public bool OverlapsWith(Room other) {
			return (left <= other.right + 1 && right >= other.left &&
				top >= other.bottom - 1 && bottom <= other.top);
		}

		public Room ClosestNonConnectedRoom(List<Room> otherRooms) {
			Room closest = null;
			float distance = Mathf.Infinity;

			for (int i = 0; i < otherRooms.Count; i++) {
				if (otherRooms[i] == this || otherRooms[i].connected)
					continue;
				float dist = Vector2.Distance(new Vector2(centerX, centerY), new Vector2(otherRooms[i].centerX, otherRooms[i].centerY));
				if (dist < distance) {
					closest = otherRooms[i];
					distance = dist;
					connected = true;
				}
			}
			if (!connected)
				return ClosestRoom(otherRooms);

			return closest;
		}

		public Room ClosestRoom(List<Room> otherRooms) {
			Room closest = null;
			float distance = Mathf.Infinity;

			for (int i = 0; i < otherRooms.Count; i++) {
				if (otherRooms[i] == this)
					continue;
				float dist = Vector2.Distance(new Vector2(centerX, centerY), new Vector2(otherRooms[i].centerX, otherRooms[i].centerY));
				if (dist < distance) {
					closest = otherRooms[i];
					distance = dist;
				}
			}

			connected = true;
			return closest;
		}
	}
}
