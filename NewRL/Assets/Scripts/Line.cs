﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Line {
	Coord c0;
	Coord c1;
	List<Coord> points;

	public Line(Coord _c0, Coord _c1) {
		c0 = _c0;
		c1 = _c1;
		points = new List<Coord>();
	}

	public List<Coord> newLine() {
		int dx = c1.x - c0.x, dy = c1.y - c0.y;
		int nx = Mathf.Abs(dx), ny = Mathf.Abs(dy);
		int sign_x = dx > 0? 1 : -1, sign_y = dy > 0? 1 : -1;
		float error = 0.5f;
		
		Coord p = new Coord(c0.x, c0.y);

		for (int ix = 0, iy = 0; ix < nx || iy < ny;) {
            bool incremented = false;

			if ((error + ix) / nx < (error + iy) / ny) {
				p.x += sign_x;
				ix++;
                incremented = true;
			}

			if ((error + iy) / ny < (error + ix) / nx) {
				p.y += sign_y;
				iy++;
                incremented = true;
			}

			if (!incremented && (error + ix) / nx == (error + iy) / ny) {
				p.x += sign_x;
				p.y += sign_y;
				ix++;
				iy++;
			}

			points.Add(new Coord(p.x, p.y));
		}

		return points;
	}
}
