﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map {

	public static Map instance;
	public Cell[,] tiles;
	public List<Generator.Room> rooms;
	public List<Cell> walkableTiles;

	int mapSizeX;
	int mapSizeY;

	public Map(int sizeX, int sizeY) {
		Map.instance = this;
		mapSizeX = sizeX;
		mapSizeY = sizeY;
		tiles = new Cell[mapSizeX, mapSizeY];

		for (int x = 0; x < mapSizeX; x++) {
			for (int y = 0; y < mapSizeY; y++) {
				tiles[x, y] = new Cell(0, x, y);
			}
		}

		BuildMap();
	}

	public void BuildMap() {
		walkableTiles = new List<Cell>();
		for (int x = 0; x < mapSizeX; x++) {
			for (int y = 0; y < mapSizeY; y++) {
				tiles[x, y].tileID = Random.Range(0, 2);
				tiles[x, y].walkable = false;
			}
		}

		RoomsCorridors();
	}

	public void RoomsCorridors() {
		int numTries = 0, maxTries = 500;
		rooms = new List<Generator.Room>();

		while (rooms.Count < 13 && numTries < maxTries) {
			int w = Random.Range(3, 7), h = Random.Range(3, 7);
			int l = Random.Range(1, mapSizeX - w), b = Random.Range(1, mapSizeY - h);
			Generator.Room room = new Generator.Room(l, b, w, h);

			if (RoomOkayToUse(room)) {
				rooms.Add(room);
				for (int x = room.left; x < room.right; x++) {
					for (int y = room.bottom; y < room.top; y++) {
						tiles[x, y].walkable = true;
						tiles[x, y].tileID = Random.Range(2, 4);
					}
				}
			} else
				numTries ++;
		}

		for (int i = 1; i < rooms.Count; i++) {
			MakeCorridor(rooms[i], rooms[i].ClosestNonConnectedRoom(rooms));
		}

		for (int x = 0; x < mapSizeX; x++) {
			for (int y = 0; y < mapSizeY; y++) {
				if (x == 0 || y == 0 || x == mapSizeX - 1 || y == mapSizeY - 1) {
					tiles[x, y].tileID = Random.Range(0, 2);
					tiles[x, y].walkable = false;
				} else if (tiles[x, y].walkable) {
					walkableTiles.Add(tiles[x, y]);
				}
			}
		}
	}

	void MakeCorridor(Generator.Room r1, Generator.Room r2) {
		int x = Random.Range(r1.left, r1.right), x1 = Random.Range(r2.left, r2.right), 
		y = Random.Range(r1.bottom, r1.top), y1 = Random.Range(r2.bottom, r2.top);
		int xOffset = 0, yOffset = 0;

		if (x != x1)
			xOffset = (x < x1) ? 1 : -1;
		if (y != y1)
			yOffset = (y < y1) ? 1 : -1;

		if (Random.value < 0.5f) {
			while (x != x1) {
				tiles[x, y].tileID = Random.Range(2, 4);
				tiles[x, y].walkable = true;
				x += xOffset;
			}
			while (y != y1) {
				tiles[x, y].tileID = Random.Range(2, 4);
				tiles[x, y].walkable = true;
				y += yOffset;
			}
		} else {
			while (y != y1) {
				tiles[x, y].tileID = Random.Range(2, 4);
				tiles[x, y].walkable = true;
				y += yOffset;
			}
			while (x != x1) {
				tiles[x, y].tileID = Random.Range(2, 4);
				tiles[x, y].walkable = true;
				x += xOffset;
			}
		}
	}

	bool RoomOkayToUse(Generator.Room r) {
		foreach (Generator.Room rm in rooms) {
			if (rm.OverlapsWith(r))
				return false;
		}

		return true;
	}

	public bool Walkable(int x, int y) {
		if (x < 0 || x >= mapSizeX || y < 0 || y >= mapSizeY) {
			Debug.LogError("Out of map! (" + x + "," + y + ")");
			return tiles[0, 0].Walkable();
		}

		return tiles[x, y].Walkable();
	}

	public Cell GetTileAt(int x, int y) {
		if (x < 0 || x >= mapSizeX || y < 0 || y >= mapSizeY) {
			Debug.LogError("Out of map! (" + x + "," + y + ")");
			return tiles[0, 0];
		}

		return tiles[x, y];
	}
}
