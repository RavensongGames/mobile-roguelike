﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour {

	public static MapManager instance;

	public static int mapSizeX = 30;
	public static int mapSizeY = 30;

	public Sprite[] spriteGraphics;
	public GameObject tileObject;
	Map currentMap;
	SpriteRenderer[,] gos;

	void Start() {
		instance = this;
		currentMap = new Map(mapSizeX, mapSizeY);
		gos = new SpriteRenderer[mapSizeX, mapSizeY];

		Init();
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.F1))
			LightAll();
	}

	public void LOSCalc() {
		for (int x = 0; x < mapSizeX; x++) {
			for (int y = 0; y < mapSizeY; y++) {
				bool lit = inSight(x, y);
				Cell c = currentMap.GetTileAt(x, y);
				c.lit = lit;

				DisplayLight(c);
			}
		}
	}

	bool inSight(int x, int y) {
		int cx = EntityManager.playerInstance.Key.posX, cy = EntityManager.playerInstance.Key.posY;
		int dx = x - cx, dy = y - cy;
		int nx = Mathf.Abs(dx), ny = Mathf.Abs(dy);
		int sign_x = dx > 0 ? 1 : -1, sign_y = dy > 0 ? 1 : -1;

		int px = cx, py = cy;
		for (int ix = 0, iy = 0; ix < nx || iy < ny;) {
			if (!Map.instance.GetTileAt(px, py).walkable)
				return false;
			
			float fx = (0.5f + ix) / nx, fy = (0.5f + iy) / ny;

			if (fx == fy) {
				px += sign_x;
				py += sign_y;
				ix++;
				iy++;
			} else if (fx < fy) {
				px += sign_x;
				ix++;
			} else {
				py += sign_y;
				iy++;
			}
		}

		return true;
	}

	void LightAll() {
		for (int x = 0; x < mapSizeX; x++) {
			for (int y = 0; y < mapSizeY; y++) {
				bool lit = true;
				Cell c = currentMap.GetTileAt(x, y);
				c.lit = lit;

				DisplayLight(c);
			}
		}
	}

	void DisplayLight(Cell c) {
		if (c.lit) {
			c.hasSeen = true;
			gos[c.x, c.y].color = Color.white;
		} else {
			gos[c.x, c.y].color = (c.hasSeen) ? Color.grey : Color.black;
		}
	}

	void Init() {
		for (int x = 0; x < mapSizeX; x++) {
			for (int y = 0; y < mapSizeY; y++) {
				gos[x, y] = Instantiate(tileObject, new Vector3(x, y, 0), Quaternion.identity, this.transform).GetComponent<SpriteRenderer>();
				gos[x, y].sprite = spriteGraphics[currentMap.GetTileAt(x, y).tileID];
			}
		}

		GameObject.FindObjectOfType<EntityManager>().Init();
	}

	public Cell GetOpenTile() {
		Cell c = currentMap.walkableTiles[Random.Range(0, currentMap.walkableTiles.Count)];
		currentMap.walkableTiles.Remove(c);
		return c;
	}

	public void NewMap() {
		currentMap.BuildMap();
	}
}
