﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageLog : MonoBehaviour {

	public static MessageLog instance;
	public GameObject textObject;
	public Text[] messages;

	void Start() {
		instance = this;
		messages[2].text = "";
		messages[1].text = "";
		messages[0].text = "";
	}

	public void NewMessage(string message) {
		messages[2].text = messages[1].text;
		messages[1].text = messages[0].text;
		messages[0].text = message;
	}
}
