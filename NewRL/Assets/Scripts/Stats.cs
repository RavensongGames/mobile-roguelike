﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats {

	public Entity entity;

	public int maxHP;
	public int HP;

	public int defense;
	public int strength;
	public int speed;

	public Stats(int mHP, int def, int str, int spd = 10) {
		maxHP = mHP;
		HP = maxHP;
		defense = def;
		strength = str;
		speed = spd;
	}

	public void TakeDamage(int amount, Entity attacker) {
		int total = amount - defense;

		if (total > 0) {
			HP -= total;
			MessageLog.instance.NewMessage(string.Format("{0} deals <color=yellow>{1}</color> damage to {2}.", attacker.Name, total.ToString(), entity.Name));
		} else
			MessageLog.instance.NewMessage(string.Format("{0}'s attack is blocked by {1}.", attacker.Name, entity.Name));
		
		if (HP <= 0) 
			entity.Die();
	}
}
