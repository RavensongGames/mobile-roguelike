﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_HPBar : MonoBehaviour {

	public Image hpBar;
	public Text hpText;
	Vector3 scale;

	void Start() {
		scale = new Vector3(1, 1, 1);
	}


	void Update() {
		if (EntityManager.playerInstance.Key.stats != null)
			DisplayHP();
	}

	void DisplayHP() {
		scale.x = ((float)EntityManager.playerInstance.Key.stats.HP / (float)EntityManager.playerInstance.Key.stats.maxHP);
		scale.x = Mathf.Clamp(scale.x, 0.001f, 1);
		hpBar.rectTransform.localScale = scale;
		hpText.text = EntityManager.playerInstance.Key.stats.HP + " / " + EntityManager.playerInstance.Key.stats.maxHP;
	}
}
